#include "stdafx.h"
#include "Mesh.h"
#include <math.h>
#include<iostream>
#define PI			3.1415926
#define	COLORNUM		16
const float d2r = 3.14159f / 180;

float	ColorArr[COLORNUM][3] = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 },
{ 1.0, 1.0, 0.0 }, { 1.0, 0.0, 1.0 }, { 0.0, 1.0, 1.0 },
{ 0.3, 0.3, 1.0 }, { 0.5, 1.0, 0.5 }, { 0.9, 0.9, 0.9 },
{ 1.0, 0.5, 0.5 }, { 0.75, 1.0, 0.25 }, { 0.5, 0.5, 1.0 },
{ 1.0, 0.4, 0.0 }, { 0.2, 1.0, 1.0 }, {0.8,0.2,0.3}, {0.8,0.8,0.0} };





void Mesh::CreateCube(float	fSize)
{
	int i;

	numVerts=8;
	pt = new Point3[numVerts];
	pt[0].set(-fSize, fSize, fSize);
	pt[1].set( fSize, fSize, fSize);
	pt[2].set( fSize, fSize, -fSize);
	pt[3].set(-fSize, fSize, -fSize);
	pt[4].set(-fSize, -fSize, fSize);
	pt[5].set( fSize, -fSize, fSize);
	pt[6].set( fSize, -fSize, -fSize);
	pt[7].set(-fSize, -fSize, -fSize);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	for(i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 0;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;
	for(i = 0; i<face[1].nVerts ; i++)
		face[1].vert[i].colorIndex = 1;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;
	for(i = 0; i<face[2].nVerts ; i++)
		face[2].vert[i].colorIndex = 2;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;
	for(i = 0; i<face[4].nVerts ; i++)
		face[4].vert[i].colorIndex = 4;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;
	for(i = 0; i<face[5].nVerts ; i++)
		face[5].vert[i].colorIndex = 5;

}


void Mesh::CreateTetrahedron()
{
	int i;
	numVerts=4;
	pt = new Point3[numVerts];
	pt[0].set(0, 0, 0);
	pt[1].set(1, 0, 0);
	pt[2].set(0, 1, 0);
	pt[3].set(0, 0, 1);

	numFaces= 4;
	face = new Face[numFaces];

	face[0].nVerts = 3;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 2;
	face[0].vert[2].vertIndex = 3;
	for(i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 0;
	

	face[1].nVerts = 3;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;	
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	for(i = 0; i<face[1].nVerts ; i++)
		face[1].vert[i].colorIndex = 1;

	
	face[2].nVerts = 3;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 3;
	face[2].vert[2].vertIndex = 2;
	for(i = 0; i<face[2].nVerts ; i++)
		face[2].vert[i].colorIndex = 2;


	face[3].nVerts = 3;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 1;
	face[3].vert[1].vertIndex = 3;
	face[3].vert[2].vertIndex = 0;
	for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;
}


void Mesh::DrawWireframe()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::DrawColor()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			int		ic = face[f].vert[v].colorIndex;
			
			ic %= COLORNUM;
			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]); 
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ){
	//Vertices
	numVerts = 8;
	pt = (Point3*)calloc(numVerts, sizeof(Point3));
	pt[0].set(fSizeX, -fSizeY, fSizeZ);
	pt[1].set(fSizeX, -fSizeY, -fSizeZ);
	pt[2].set(-fSizeX, -fSizeY, -fSizeZ);
	pt[3].set(-fSizeX, -fSizeY, fSizeZ);
	pt[4].set(fSizeX, fSizeY, fSizeZ);
	pt[5].set(fSizeX, fSizeY, -fSizeZ);
	pt[6].set(-fSizeX, fSizeY, -fSizeZ);
	pt[7].set(-fSizeX, fSizeY, fSizeZ);
	//Faces
	numFaces = 6;
	face = (Face*)calloc(numFaces, sizeof(Face));
	//face 0 - right
	face[0].nVerts = 4;
	face[0].vert = (VertexID*)calloc(face[0].nVerts, sizeof(VertexID));
	face[0].vert[0].vertIndex = 3;
	face[0].vert[1].vertIndex = 7;
	face[0].vert[2].vertIndex = 4;
	face[0].vert[3].vertIndex = 0;
	/*for (int i = 0; i < face[0].nVerts; i++){
		face[0].vert[i].colorIndex = 0;
	}*/
	//face 1 - front
	face[1].nVerts = 4;
	face[1].vert = (VertexID*)calloc(face[1].nVerts, sizeof(VertexID));
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 4;
	face[1].vert[2].vertIndex = 5;
	face[1].vert[3].vertIndex = 1;
	/*for (int i = 0; i < face[1].nVerts; i++){
		face[1].vert[i].colorIndex = 1;
	}*/
	//face 2 - left
	face[2].nVerts = 4;
	face[2].vert = (VertexID*)calloc(face[2].nVerts, sizeof(VertexID));
	face[2].vert[0].vertIndex = 1;
	face[2].vert[1].vertIndex = 5;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[3].vertIndex = 2;
	/*for (int i = 0; i < face[2].nVerts; i++){
		face[2].vert[i].colorIndex = 2;
	}*/
	//face 3 - behind
	face[3].nVerts = 4;
	face[3].vert = (VertexID*)calloc(face[3].nVerts, sizeof(VertexID));
	face[3].vert[0].vertIndex = 2;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 7;
	face[3].vert[3].vertIndex = 3;
	/*for (int i = 0; i < face[3].nVerts; i++){
		face[3].vert[i].colorIndex = 3;
	}*/
	//face 4 - bottom
	face[4].nVerts = 4;
	face[4].vert = (VertexID*)calloc(face[4].nVerts, sizeof(VertexID));
	face[4].vert[0].vertIndex = 3;
	face[4].vert[1].vertIndex = 0;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 2;
	/*for (int i = 0; i < face[4].nVerts; i++){
		face[4].vert[i].colorIndex = 4;
	}*/
	//face 5 - top
	face[5].nVerts = 4;
	face[5].vert = (VertexID*)calloc(face[5].nVerts, sizeof(VertexID));
	face[5].vert[0].vertIndex = 7;
	face[5].vert[1].vertIndex = 6;
	face[5].vert[2].vertIndex = 5;
	face[5].vert[3].vertIndex = 4;
	/*for (int i = 0; i < face[5].nVerts; i++){
		face[5].vert[i].colorIndex = 5;
	}*/
	CalculateFacesNorm();
}


void Mesh::createCuboidWithHole(float	fLSizeX, float fLSizeY, float	fLSizeZ, float	fSSizeX, float fSSizeY, float fSSizeZ){
	//Vertices
	numVerts = 16;
	pt = (Point3*)calloc(numVerts, sizeof(Point3));
	pt[0].set(fLSizeX, -fLSizeY, fLSizeZ);
	pt[1].set(fLSizeX, -fLSizeY, -fLSizeZ);
	pt[2].set(-fLSizeX, -fLSizeY, -fLSizeZ);
	pt[3].set(-fLSizeX, -fLSizeY, fLSizeZ);
	pt[4].set(fLSizeX, fLSizeY, fLSizeZ);
	pt[5].set(fLSizeX, fLSizeY, -fLSizeZ);
	pt[6].set(-fLSizeX, fLSizeY, -fLSizeZ);
	pt[7].set(-fLSizeX, fLSizeY, fLSizeZ);
	pt[8].set(fSSizeX, -fSSizeY, fSSizeZ);
	pt[9].set(fSSizeX, -fSSizeY, -fSSizeZ);
	pt[10].set(-fSSizeX, -fSSizeY, -fSSizeZ);
	pt[11].set(-fSSizeX, -fSSizeY, fSSizeZ);
	pt[12].set(fSSizeX, fSSizeY, fSSizeZ);
	pt[13].set(fSSizeX, fSSizeY, -fSSizeZ);
	pt[14].set(-fSSizeX, fSSizeY, -fSSizeZ);
	pt[15].set(-fSSizeX, fSSizeY, fSSizeZ);

	//Faces
	numFaces = 16;
	face = (Face*)calloc(numFaces, sizeof(Face));

	//Larger cuboid
	//face 0 - right
	face[0].nVerts = 4;
	face[0].vert = (VertexID*)calloc(face[0].nVerts, sizeof(VertexID));
	face[0].vert[0].vertIndex = 3;
	face[0].vert[1].vertIndex = 7;
	face[0].vert[2].vertIndex = 4;
	face[0].vert[3].vertIndex = 0;
	for (int i = 0; i < face[0].nVerts; i++){
		face[0].vert[i].colorIndex = 0;
	}
	//face 1 - front
	face[1].nVerts = 4;
	face[1].vert = (VertexID*)calloc(face[1].nVerts, sizeof(VertexID));
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 4;
	face[1].vert[2].vertIndex = 5;
	face[1].vert[3].vertIndex = 1;
	for (int i = 0; i < face[1].nVerts; i++){
		face[1].vert[i].colorIndex = 1;
	}
	//face 2 - left
	face[2].nVerts = 4;
	face[2].vert = (VertexID*)calloc(face[2].nVerts, sizeof(VertexID));
	face[2].vert[0].vertIndex = 1;
	face[2].vert[1].vertIndex = 5;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[3].vertIndex = 2;
	for (int i = 0; i < face[2].nVerts; i++){
		face[2].vert[i].colorIndex = 2;
	}
	//face 3 - behind
	face[3].nVerts = 4;
	face[3].vert = (VertexID*)calloc(face[3].nVerts, sizeof(VertexID));
	face[3].vert[0].vertIndex = 2;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 7;
	face[3].vert[3].vertIndex = 3;
	for (int i = 0; i < face[3].nVerts; i++){
		face[3].vert[i].colorIndex = 3;
	}
	//Smaller cuboid
	//face 4 - right
	face[4].nVerts = 4;
	face[4].vert = (VertexID*)calloc(face[4].nVerts, sizeof(VertexID));
	face[4].vert[0].vertIndex = 11;
	face[4].vert[1].vertIndex = 15;
	face[4].vert[2].vertIndex = 12;
	face[4].vert[3].vertIndex = 8;
	for (int i = 0; i < face[4].nVerts; i++){
		face[4].vert[i].colorIndex = 4;
	}
	//face 5 - front
	face[5].nVerts = 4;
	face[5].vert = (VertexID*)calloc(face[5].nVerts, sizeof(VertexID));
	face[5].vert[0].vertIndex = 8;
	face[5].vert[1].vertIndex = 12;
	face[5].vert[2].vertIndex = 13;
	face[5].vert[3].vertIndex = 9;
	for (int i = 0; i < face[5].nVerts; i++){
		face[5].vert[i].colorIndex = 5;
	}
	//face 6 - left
	face[6].nVerts = 4;
	face[6].vert = (VertexID*)calloc(face[6].nVerts, sizeof(VertexID));
	face[6].vert[0].vertIndex = 9;
	face[6].vert[1].vertIndex = 13;
	face[6].vert[2].vertIndex = 14;
	face[6].vert[3].vertIndex = 10;
	for (int i = 0; i < face[6].nVerts; i++){
		face[6].vert[i].colorIndex = 6;
	}
	//face 7 - behind
	face[7].nVerts = 4;
	face[7].vert = (VertexID*)calloc(face[7].nVerts, sizeof(VertexID));
	face[7].vert[0].vertIndex = 10;
	face[7].vert[1].vertIndex = 14;
	face[7].vert[2].vertIndex = 15;
	face[7].vert[3].vertIndex = 11;
	for (int i = 0; i < face[7].nVerts; i++){
		face[7].vert[i].colorIndex = 7;
	}
	//Bottom face
	//face 8 - right
	face[8].nVerts = 4;
	face[8].vert = (VertexID*)calloc(face[8].nVerts, sizeof(VertexID));
	face[8].vert[0].vertIndex = 11;
	face[8].vert[1].vertIndex = 3;
	face[8].vert[2].vertIndex = 0;
	face[8].vert[3].vertIndex = 8;
	for (int i = 0; i < face[8].nVerts; i++){
		face[8].vert[i].colorIndex = 8;
	}
	//face 9 - front
	face[9].nVerts = 4;
	face[9].vert = (VertexID*)calloc(face[9].nVerts, sizeof(VertexID));
	face[9].vert[0].vertIndex = 8;
	face[9].vert[1].vertIndex = 0;
	face[9].vert[2].vertIndex = 1;
	face[9].vert[3].vertIndex = 9;
	for (int i = 0; i < face[9].nVerts; i++){
		face[9].vert[i].colorIndex = 9;
	}
	//face 10 - left
	face[10].nVerts = 4;
	face[10].vert = (VertexID*)calloc(face[10].nVerts, sizeof(VertexID));
	face[10].vert[0].vertIndex = 9;
	face[10].vert[1].vertIndex = 1;
	face[10].vert[2].vertIndex = 2;
	face[10].vert[3].vertIndex = 10;
	for (int i = 0; i < face[10].nVerts; i++){
		face[10].vert[i].colorIndex = 10;
	}
	//face 11 - behind
	face[11].nVerts = 4;
	face[11].vert = (VertexID*)calloc(face[11].nVerts, sizeof(VertexID));
	face[11].vert[0].vertIndex = 10;
	face[11].vert[1].vertIndex = 2;
	face[11].vert[2].vertIndex = 3;
	face[11].vert[3].vertIndex = 11;
	for (int i = 0; i < face[11].nVerts; i++){
		face[11].vert[i].colorIndex = 11;
	}
	//Top face
	//face 12 - right
	face[12].nVerts = 4;
	face[12].vert = (VertexID*)calloc(face[12].nVerts, sizeof(VertexID));
	face[12].vert[0].vertIndex = 7;
	face[12].vert[1].vertIndex = 15;
	face[12].vert[2].vertIndex = 12;
	face[12].vert[3].vertIndex = 4;
	for (int i = 0; i < face[12].nVerts; i++){
		face[12].vert[i].colorIndex = 12;
	}
	//face 13 - front
	face[13].nVerts = 4;
	face[13].vert = (VertexID*)calloc(face[13].nVerts, sizeof(VertexID));
	face[13].vert[0].vertIndex = 4;
	face[13].vert[1].vertIndex = 12;
	face[13].vert[2].vertIndex = 13;
	face[13].vert[3].vertIndex = 5;
	for (int i = 0; i < face[13].nVerts; i++){
		face[13].vert[i].colorIndex = 13;
	}
	//face 14 - left
	face[14].nVerts = 4;
	face[14].vert = (VertexID*)calloc(face[14].nVerts, sizeof(VertexID));
	face[14].vert[0].vertIndex = 5;
	face[14].vert[1].vertIndex = 13;
	face[14].vert[2].vertIndex = 14;
	face[14].vert[3].vertIndex = 6;
	for (int i = 0; i < face[14].nVerts; i++){
		face[14].vert[i].colorIndex = 14;
	}
	//face 15 - behind
	face[15].nVerts = 4;
	face[15].vert = (VertexID*)calloc(face[15].nVerts, sizeof(VertexID));
	face[15].vert[0].vertIndex = 6;
	face[15].vert[1].vertIndex = 14;
	face[15].vert[2].vertIndex = 15;
	face[15].vert[3].vertIndex = 7;
	for (int i = 0; i < face[15].nVerts; i++){
		face[15].vert[i].colorIndex = 14;
	}
	CalculateFacesNorm();
}


void Mesh::CreateCylinder(float fRadius, float fHeight, int numOfPoint){
	//color
	//int color = 0;
	//Vertices
	numVerts = numOfPoint * 2 + 2;
	pt = (Point3*)calloc(numVerts, sizeof(Point3));
	
	//top circle
	for (int i = 0; i < numOfPoint; i++){
		pt[i].set(fRadius*sinf(d2r*i * (360 / numOfPoint)), fHeight, fRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//bottom circle
	for (int i = 0; i < numOfPoint; i++){
		pt[i + numOfPoint].set(fRadius*sinf(d2r*i * (360 / numOfPoint)), -fHeight, fRadius*cosf(d2r*i * (360 / numOfPoint)));
	}
	pt[numVerts - 2].set(0, fHeight, 0);
	pt[numVerts - 1].set(0, -fHeight, 0);
	//Faces
	numFaces = numOfPoint*3;
	face = (Face*)calloc(numFaces, sizeof(Face));
	int faceOrder = 0;
	for (int i = 0; i < numOfPoint; i++){
		face[faceOrder].nVerts = 3;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = numVerts - 2;
		face[faceOrder].vert[1].vertIndex = i;
		face[faceOrder].vert[2].vertIndex = (i + 1)%numOfPoint;

		faceOrder++;
		face[faceOrder].nVerts = 4;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = i;
		face[faceOrder].vert[1].vertIndex = (i + 1)%numOfPoint;
		face[faceOrder].vert[2].vertIndex = (i + 1 + numOfPoint) % (numOfPoint * 2) == 0 ? numOfPoint : (i + 1 + numOfPoint);
		face[faceOrder].vert[3].vertIndex = i + numOfPoint;
		

		faceOrder++;
		face[faceOrder].nVerts = 3;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = numVerts - 1;
		face[faceOrder].vert[1].vertIndex = i + numOfPoint;
		face[faceOrder].vert[2].vertIndex = (i + 1 + numOfPoint) % (numOfPoint * 2) == 0 ? numOfPoint : (i + 1 + numOfPoint);
	
		faceOrder++;
	}
	CalculateFacesNorm();
}


void Mesh::CreateOval(float fRadius, float fLength, float fHeight, int numOfPoint){
	//color
	int color = 0;
	//Vertices
	numVerts = numOfPoint * 2;
	pt = (Point3*)calloc(numVerts, sizeof(Point3));

	//top circle - front
	for (int i = 0; i < numOfPoint/2; i++){
		pt[i].set(fLength+fRadius*sinf(d2r*i * (360 / numOfPoint)), fHeight, fRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//top circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + numOfPoint / 2].set(-fLength + fRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), fHeight, fRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}
	//bottom circle - front
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i+numOfPoint].set(fLength + fRadius*sinf(d2r*i * (360 / numOfPoint)), -fHeight, fRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//bottom circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + (numOfPoint * 3) / 2].set(-fLength + fRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), -fHeight, fRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}

	//Faces
	numFaces = numOfPoint + 2; //+2: top and bottom face
	face = (Face*)calloc(numFaces, sizeof(Face));
	for (int i = 0; i < numOfPoint; i++){
		face[i].nVerts = 4;
		face[i].vert = (VertexID*)calloc(face[i].nVerts, sizeof(VertexID));
		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = (i + 1) % numOfPoint;
		face[i].vert[2].vertIndex = (i + 1 + numOfPoint) % (2 * numOfPoint) == 0 ?  numOfPoint : (i + 1 + numOfPoint);
		face[i].vert[3].vertIndex = i+numOfPoint;
		for (int j = 0; j < face[i].nVerts; j++)
			face[i].vert[j].colorIndex = color++ % 16;
	}
	//top face
	face[numOfPoint].nVerts = numOfPoint;
	face[numOfPoint].vert = (VertexID*)calloc(face[numOfPoint].nVerts, sizeof(VertexID));
	for (int i = 0; i < numOfPoint; i++){
		face[numOfPoint].vert[i].vertIndex = i;
		face[numOfPoint].vert[i].colorIndex = color++ % 16;
	}
	//bottom face
	face[numOfPoint+1].nVerts = numOfPoint;
	face[numOfPoint+1].vert = (VertexID*)calloc(face[numOfPoint+1].nVerts, sizeof(VertexID));
	for (int i = 0; i < numOfPoint; i++){
		face[numOfPoint+1].vert[i].vertIndex = i+numOfPoint;
		face[numOfPoint+1].vert[i].colorIndex = color++ % 16;
	}
	CalculateFacesNorm();
}

void Mesh::CreateOvalWithHole(float fLRadius, float fSRadius, float fLLength, float fSLength, float fHeight, int numOfPoint){
	//color
	int color = 0;
	//Vertices
	numVerts = numOfPoint * 4;
	pt = (Point3*)calloc(numVerts, sizeof(Point3));

	//top large circle - front
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i].set(fLLength + fLRadius*sinf(d2r*i * (360 / numOfPoint)), fHeight, fLRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//top large circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + numOfPoint / 2].set(-fLLength + fLRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), fHeight, fLRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}
	//bottom large circle - front
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + numOfPoint].set(fLLength + fLRadius*sinf(d2r*i * (360 / numOfPoint)), -fHeight, fLRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//bottom large circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + (numOfPoint * 3) / 2].set(-fLLength + fLRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), -fHeight, fLRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}


	//top small circle - front
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i+numOfPoint*2].set(fSLength + fSRadius*sinf(d2r*i * (360 / numOfPoint)), fHeight, fSRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//top small circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + numOfPoint*5 / 2].set(-fSLength + fSRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), fHeight, fSRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}
	//bottom small circle - front
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + numOfPoint*3].set(fSLength + fSRadius*sinf(d2r*i * (360 / numOfPoint)), -fHeight, fSRadius*cosf(d2r*i* (360 / numOfPoint)));
	}
	//bottom circle - behind
	for (int i = 0; i < numOfPoint / 2; i++){
		pt[i + (numOfPoint * 7) / 2].set(-fSLength + fSRadius*sinf(d2r*(i * (360 / numOfPoint) + 180)), -fHeight, fSRadius*cosf(d2r*(i * (360 / numOfPoint) + 180)));
	}
	//Faces
	int faceOrder = 0;
	numFaces = numOfPoint * 4;
	face = (Face*)calloc(numFaces, sizeof(Face));
	
	for (int i = 0; i < numOfPoint; i++){
		//1st face - top
		face[faceOrder].nVerts = 4;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = i;
		face[faceOrder].vert[1].vertIndex = (i+1)%numOfPoint;
		face[faceOrder].vert[2].vertIndex = (i + numOfPoint * 2 + 1) % (3 * numOfPoint) == 0 ? numOfPoint * 2 : i + numOfPoint * 2 + 1;
		face[faceOrder].vert[3].vertIndex = i + numOfPoint * 2;
		for (int j = 0; j < face[faceOrder].nVerts; j++)
			face[faceOrder].vert[j].colorIndex = color++ % 16;
		faceOrder++;
		//2nd face - body outside
		face[faceOrder].nVerts = 4;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = i;
		face[faceOrder].vert[1].vertIndex = (i + 1) % numOfPoint;
		face[faceOrder].vert[2].vertIndex = (i + numOfPoint + 1) % ( 2*numOfPoint) == 0 ? numOfPoint: i + numOfPoint + 1;
		face[faceOrder].vert[3].vertIndex = i + numOfPoint;
		for (int j = 0; j < face[faceOrder].nVerts; j++)
			face[faceOrder].vert[j].colorIndex = color++ % 16;
		faceOrder++;
		//3rd face - body inside
		face[faceOrder].nVerts = 4;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = i + numOfPoint * 2;
		face[faceOrder].vert[1].vertIndex = (i + numOfPoint * 2 + 1) % (3 * numOfPoint) == 0 ? (numOfPoint * 2) : (i + numOfPoint * 2 + 1);
		face[faceOrder].vert[2].vertIndex = (i + numOfPoint * 3 + 1) % (4 * numOfPoint) == 0 ? (numOfPoint * 3) : (i + numOfPoint * 3 + 1);
		face[faceOrder].vert[3].vertIndex = i + numOfPoint * 3;
		for (int j = 0; j < face[faceOrder].nVerts; j++)
			face[faceOrder].vert[j].colorIndex = color++ % 16;
		faceOrder++;
		//4nd face - bottom
		face[faceOrder].nVerts = 4;
		face[faceOrder].vert = (VertexID*)calloc(face[faceOrder].nVerts, sizeof(VertexID));
		face[faceOrder].vert[0].vertIndex = i + numOfPoint;
		face[faceOrder].vert[1].vertIndex = (i + numOfPoint  + 1) % ( 2*numOfPoint) == 0 ? numOfPoint  : (i + numOfPoint + 1);
		face[faceOrder].vert[2].vertIndex = (i + numOfPoint * 3 + 1) % (4 * numOfPoint) == 0 ? (numOfPoint * 3 ): (i + numOfPoint * 3 + 1);
		face[faceOrder].vert[3].vertIndex = i + numOfPoint * 3;
		for (int j = 0; j < face[faceOrder].nVerts; j++)
			face[faceOrder].vert[j].colorIndex = color++ % 16;
		faceOrder++;
	}
	CalculateFacesNorm();
}
//Lab 3
void Mesh::SetColor(int colorIdx){
	for (int f = 0; f < numFaces; f++){
		for (int v = 0; v < face[f].nVerts; v++){
			face[f].vert[v].colorIndex = colorIdx;
		}
	}
}
//Lab 5
void Mesh::Draw() {
	for (int f = 0; f < numFaces; f++){
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++){
			int		iv = face[f].vert[v].vertIndex;
			glNormal3f(face[f].facenorm.x, face[f].facenorm.y, face[f].facenorm.z);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::CalculateFacesNorm(){
	int nVerts = 0;
	for (int i = 0; i < numFaces; i++){
		face[i].facenorm.x = face[i].facenorm.y = face[i].facenorm.z = 0;
		nVerts = face[i].nVerts;
		for (int j = 0; j < nVerts; j++){
			face[i].facenorm.x += (pt[face[i].vert[j].vertIndex].y - pt[face[i].vert[(j + 1) % nVerts].vertIndex].y)*(pt[face[i].vert[j].vertIndex].z + pt[face[i].vert[(j + 1) % nVerts].vertIndex].z);
			face[i].facenorm.y += (pt[face[i].vert[j].vertIndex].z - pt[face[i].vert[(j + 1) % nVerts].vertIndex].z)*(pt[face[i].vert[j].vertIndex].x + pt[face[i].vert[(j + 1) % nVerts].vertIndex].x);
			face[i].facenorm.z += (pt[face[i].vert[j].vertIndex].x - pt[face[i].vert[(j + 1) % nVerts].vertIndex].x)*(pt[face[i].vert[j].vertIndex].y + pt[face[i].vert[(j + 1) % nVerts].vertIndex].y);
		}	
		face[i].facenorm.normalize();
	}
}

