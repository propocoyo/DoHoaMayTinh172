#if !defined (_MESH_CLASS)
#define _MESH_CLASS

#include "supportClass.h"


class VertexID
{
public:
	int		vertIndex;
	int		colorIndex;
};

class Face
{
public:
	int		nVerts;
	VertexID*	vert;
	Vector3		facenorm;
	
	Face()
	{
		nVerts	= 0;
		vert	= NULL;
	}
	~Face()
	{
		if(vert !=NULL)
		{
			delete[] vert;
			vert = NULL;
		}
		nVerts = 0;
	}
};

class Mesh
{
public:
	int		numVerts;
	Point3*		pt;
	
	int		numFaces;
	Face*		face;
	//Lab3 new attributes
	float		slideX, slideY, slideZ;
	float		rotateX, rotateY, rotateZ;
	float		scaleX, scaleY, scaleZ;

public:
	Mesh()
	{
		numVerts	= 0;
		pt		= NULL;
		numFaces	= 0;
		face		= NULL;
		slideX = slideY = slideZ = 0;
		rotateX = rotateY = rotateZ = 0;
		scaleX = scaleY = scaleZ = 1;
	}
	~Mesh()
	{
		if (pt != NULL)
		{
			delete[] pt;
		}	
		if(face != NULL)
		{
//			delete[] face;
		}
		numVerts = 0;
		numFaces = 0;
	}
	//Lab2
	void DrawWireframe();
	void DrawColor();
	
	void CreateTetrahedron();
	void CreateCube(float	fSize);
	void CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ);
	void createCuboidWithHole(float	fLSizeX, float fLSizeY, float fLSizeZ, float	fSSizeX, float fSSizeY, float fSSizeZ);
	void CreateCylinder(float fRadius, float fHeight, int numOfPoint);
	void CreateOval(float fRadius, float fLength, float fHeight, int numOfPoint);
	void CreateOvalWithHole(float fLRadius,float fSRadius, float fLLength,float FSLength, float fHeight, int numOfPoint);

	//Lab 3 new function
	void SetColor(int colorIdx);

	//Lab 5
	void Draw();
	void CalculateFacesNorm();

};

#endif