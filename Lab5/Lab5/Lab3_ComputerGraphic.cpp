// Lab3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"
#include "Tga.h"
using namespace std;

#define PI			3.1415926

int		screenWidth = 600;
int		screenHeight = 600;

bool	bWireFrame = 0;

float	baseRadius = 0.6;
float	baseHeight = 0.1;
float	baseRotateStep = 5;

float	columnSizeX = 0.1;
float	columnSizeZ = columnSizeX;
float	columnSizeY = 2;
float	previous = 0;


Mesh	base;
Mesh	column;
Mesh	ovalT1;
Mesh	columnT1;
Mesh	ovalT2;
Mesh	columnT2;
Mesh	cuboicShelf1;
Mesh	cuboicWithHoleShelf1;
Mesh	cuboic1Shelf2;
Mesh	cuboic2Shelf2;
Mesh	cuboicWithHoleShelf2;
Mesh	cuboic1Crank;
Mesh	cuboic2Crank;
Mesh	ovalCrank;
float d2r = 3.14159f / 180;
//Lab 4 attributes for setting camera
float camera_angle;
float camera_height;
float camera_dis;
float camera_X, camera_Y, camera_Z;
float lookAt_X, lookAt_Y, lookAt_Z;
bool	b4View = false;

//Lab 5
bool switchSecondLight = !false;
Texture   floorTex;

//Lab 5: Setting material for objects
void setupMaterial(float ambient[], float diffuse[], float specular[], float shiness)
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiness);
}
void loadTextures(void)	{
	bool status = LoadTGA(&floorTex, "marble.tga");
	//cout << status << endl;
	if (status) {
		glGenTextures(1, &floorTex.texID);
		glBindTexture(GL_TEXTURE_2D, floorTex.texID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floorTex.width,
			floorTex.height, 0,
			GL_RGB, GL_UNSIGNED_BYTE, floorTex.imageData);

		if (floorTex.imageData)
			free(floorTex.imageData);
	}
}

void drawFloor(){
	
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	loadTextures();
	glBindTexture(GL_TEXTURE_2D, floorTex.texID);
	glColor4f(1, 1, 1, 1.0);
	glBegin(GL_POLYGON);
	glTexCoord2f(0, 0);
	glVertex3f(10, -0.001, 10); 
	glTexCoord2f(0, 1);
	glVertex3f(10, -0.001, -10);
	glTexCoord2f(1, 1);
	glVertex3f(-10, -0.001, -10);
	glTexCoord2f(1, 0);
	glVertex3f(-10, -0.001, 10);
	glEnd();



	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);

}


//Draw coordinate axis
void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);//x
	glVertex3f(4, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);//y
	glVertex3f(0, 4, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);//z
	glVertex3f(0, 0, 4);
	glEnd();
}
//Draw components
void drawBase()
{
	glPushMatrix();

	glTranslated(0, baseHeight, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		base.DrawWireframe();
	else
	{//base.DrawColor();
		GLfloat ambient[4] = { 0.0, 1.0, 0.0, 1.0 };
		GLfloat diffuse[4] = { 0.5, 1.0, 0.5, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine= 100.0;
		setupMaterial(ambient,diffuse,specular,shine);
		base.Draw();
		
	}
	glPopMatrix();
}
void drawColumn()
{
	glPushMatrix();

	glTranslated(0, baseHeight + columnSizeY, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		column.DrawWireframe();
	else{
		//column.DrawColor();
		GLfloat ambient[4] = { 1.0, 0.0, 0.0, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.0, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		column.Draw();
	}
	glPopMatrix();
}
void drawT1()
{
	glPushMatrix();
	glTranslatef(0, -(0.4 - 0.4*cos(ovalCrank.rotateZ*d2r)), 0);
	
	glPushMatrix();
	glTranslated(0, baseHeight  + columnSizeY+0.1 -0.1, 0.2*2+0.1*2+0.15-0.1);
	//glRotatef(base.rotateY, 0, 1, 0);
	glRotatef(90, 1, 0, 0);
	/*if (ovalCrank.rotateZ<=180)
		glTranslated(0, -0.1,0);
	else glTranslated(0, 0.1, 0);*/
	if (bWireFrame){
		ovalT1.DrawWireframe();
		//columnT1.DrawWireframe();
	}
		
	else
	{
		//ovalT1.DrawColor();
		GLfloat ambient[4] = { 0.0, 0.0, 1.0, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.3, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		ovalT1.Draw();

		//columnT1.DrawColor();
	}

	glPopMatrix();
	glPushMatrix();

	glTranslated(0, baseHeight + columnSizeY +1+0.15 -0.1 , 0.1 + 0.2 * 2 + 0.15);
	//glRotatef(base.rotateY, 0, 1, 0);
	/*if (ovalCrank.rotateZ <= 180)
		glTranslated(0, -0.1, 0);
	else glTranslated(0, 0.1, 0);*/
	if (bWireFrame){
		//ovalT1.DrawWireframe();
		columnT1.DrawWireframe();
	}

	else
	{
		//columnT1.DrawColor();
		columnT1.Draw();
	}

	glPopMatrix();
	glPopMatrix();
}
void drawT2()
{
	glPushMatrix();
	glTranslatef(-0.4*sin(ovalCrank.rotateZ*d2r), 0, 0);
	glPushMatrix();

	glTranslated(0.9 + 0.15, 0.6 + baseHeight + columnSizeY / 2, 0.1 + 0.1 * 2 + 0.15);
	glRotatef(90, 0, 0, 1);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		columnT2.DrawWireframe();
	}

	else
	{
		//columnT2.DrawColor();
		GLfloat ambient[4] = { 1.0, 1.0, 0.0, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.0, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		columnT2.Draw();
	}

	glPopMatrix();
	glPushMatrix();

	glTranslated(0, 0.6 + baseHeight + columnSizeY / 2, 0.1 + 0.1 * 2 + 0.15);
	//glRotatef(90, 0, 1, 0);
	glRotatef(90, 0, 0, 1);
	glRotatef(90, 1, 0, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		ovalT2.DrawWireframe();
		//columnT2.DrawWireframe();
	}

	else
	{
		//ovalT2.DrawColor();
		ovalT2.Draw();
	}

	glPopMatrix();
	glPopMatrix();
}
void drawShelf1()
{
	glPushMatrix();
	glTranslated(0, baseHeight  + 3*columnSizeY / 2.0, columnSizeZ+0.2);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		cuboicShelf1.DrawWireframe();
	}

	else
	{
		//cuboicShelf1.DrawColor();
		GLfloat ambient[4] = { 0.2, 0.4, 0.0, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.0, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		cuboicShelf1.Draw();
	}

	glPopMatrix();
	glPushMatrix();

	glTranslated(0, baseHeight + 3 * columnSizeY / 2.0, columnSizeZ+0.2*2+0.15);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		//cuboicShelf1.DrawWireframe();
		cuboicWithHoleShelf1.DrawWireframe();
	}

	else
	{
		//cuboicWithHoleShelf1.DrawColor();
		cuboicWithHoleShelf1.Draw();
	}

	glPopMatrix();

}
void drawShelf2()
{
	glPushMatrix();

	glTranslated(columnSizeX + 1, 0.6 + baseHeight + columnSizeY / 2, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		cuboic1Shelf2.DrawWireframe();
	}

	else
	{
		//cuboic1Shelf2.DrawColor();
		GLfloat ambient[4] = { 0.5, 0.5, 0.5, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.0, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		cuboic1Shelf2.Draw();
	}

	glPopMatrix();
	glPushMatrix();

	glTranslated(4 * 1 / 3.0 + columnSizeX, 0.6 + baseHeight + columnSizeY / 2, 0.1 * 2);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		//cuboic1Shelf2.DrawWireframe();
		cuboic2Shelf2.DrawWireframe();
		//cuboicWithHoleShelf2.DrawWireframe();
	}

	else
	{
		//cuboic2Shelf2.DrawColor();
		cuboic2Shelf2.Draw();
	}

	glPopMatrix();
	glPushMatrix();

	glTranslated(4 * 1.0 / 3 + columnSizeX, 0.6 + baseHeight + columnSizeY / 2, 0.15 + columnSizeZ + 0.1 * 2);
	glRotatef(90, 0, 0, 1);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame){
		cuboicWithHoleShelf2.DrawWireframe();
	}

	else
	{
		//cuboicWithHoleShelf2.DrawColor();
		cuboicWithHoleShelf2.Draw();
	}

	glPopMatrix();
}
void drawCrank()
{
	glPushMatrix();

	glTranslated(0, 0.4 + baseHeight + columnSizeY / 2+0.2, 0);
	glRotatef(90, 1, 0, 0);


	if (bWireFrame){
		//cuboic1Crank.DrawWireframe();
		cuboic2Crank.DrawWireframe();
		//ovalCrank.DrawWireframe();
	}

	else
	{
		//cuboic2Crank.DrawColor();
		GLfloat ambient[4] = { 1.0, 0.0, 0.2, 1.0 };
		GLfloat diffuse[4] = { 0.5, 0.0, 0.2, 1.0 };
		GLfloat specular[4] = { 0.2, 0.2, 0.2, 1.0 };
		GLfloat shine = 100.0;
		setupMaterial(ambient, diffuse, specular, shine);
		cuboic2Crank.Draw();
		
	}

	glPopMatrix();
	


	glPushMatrix();

	glTranslatef(0, baseHeight + columnSizeY / 2 + columnSizeY / 4 + 0.1, 0);
	glRotatef(ovalCrank.rotateZ, 0, 0, 1);
	glTranslatef(0, -(baseHeight + columnSizeY / 2 + columnSizeY / 4 + 0.1 - 0.2), 0);
	glPushMatrix();
	glTranslated(0, baseHeight + columnSizeY -0.2, 0.3+0.1+0.15*2);
	glRotatef(90, 1, 0, 0);
	if (bWireFrame){
		cuboic1Crank.DrawWireframe();
		//cuboic2Crank.DrawWireframe();
		//ovalCrank.DrawWireframe();
	}
	else
	{
		//cuboic1Crank.DrawColor();
		cuboic1Crank.Draw();
	}
	glPopMatrix();
	glPushMatrix();

	glTranslated(0, baseHeight + columnSizeY/2+columnSizeY/4+0.1, 0.125+0.1);
	

	glRotatef(90, 0, 0, 1);
	glRotatef(90, 1, 0, 0);
	
	if (bWireFrame){
		ovalCrank.DrawWireframe();
	}
	else
	{
		//ovalCrank.DrawColor();
		//ovalCrank.DrawColor();
		ovalCrank.Draw();
	}
	glPopMatrix();
	glPopMatrix();
}
void myInit()
{
	camera_angle = 0;
	camera_dis = 4;
	camera_height = 1;
	


	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, 1, 0.1, 100);
	//Lab5
	glEnable(GL_LIGHTING);
	GLfloat	lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat	lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat	lightAmbient[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	GLfloat light_position1[] = { 9.0f, 9.0f, 9.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	GLfloat	lightDiffuse2[] = { 0.2f, 0.0f, 0.0f, 1.0f };
	GLfloat	lightSpecular2[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat	lightAmbient2[] = { 0.1f, 0.1f, 0.1f, 1.0f };
	GLfloat light_position2[] = { -6.0f, -6.0f, -6.0f, 0.0f };
	glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDiffuse2);
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbient2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpecular2);
	glEnable(GL_LIGHT0);

}

//Special functions
void mySpecialKeyboard(int key, int x, int y){
	//cout << key << endl;
	if (key == 101){
		//up
		camera_height += 0.1;
	}
	else if (key == 103)
		camera_height -= 0.1;
	else if (key == 102)
		camera_angle -= 5;
	else if (key == 100)
		camera_angle += 5;
	
}
void myKeyboard(unsigned char key, int x, int y)
{
	//r:114,R:82,w:119,W:87,1:49,2:50,3:51,4:52, d:100, D:68
	cout << (int)key << endl;
	cout << switchSecondLight << endl;
	switch (int(key))
	{
	case 49:
		base.rotateY += baseRotateStep;
		if (base.rotateY > 360)
			base.rotateY -= 360;
		break;
	case 50:
		base.rotateY -= baseRotateStep;
		if (base.rotateY < 0)
			base.rotateY += 360;
		break;
	case 51:
		ovalCrank.rotateZ += baseRotateStep;
		if (ovalCrank.rotateZ > 360)
			ovalCrank.rotateZ -= 360;
		break;
	case 52:
		ovalCrank.rotateZ -= baseRotateStep;
		if (ovalCrank.rotateZ < 0)
			ovalCrank.rotateZ += 360;
		break;
	case 114:
	case 82:
		base.rotateY = 0;
		ovalCrank.rotateZ = 0;
		break;
	case 119:
	case 87:
		bWireFrame = !bWireFrame;
		cout << bWireFrame << endl;
		break;
	case 86:
	case 118:
		b4View = !b4View;
		break;
	case 43:
		camera_dis += 0.1;
		break;
	case 45:
		camera_dis -= 0.1;
		break;
	case 100:
	case 68:
		switchSecondLight = !switchSecondLight;
		break;
	}
	glutPostRedisplay();
}
void myDisplay()
{
	if (switchSecondLight){
		
		glEnable(GL_LIGHT1);
	}
	else{
		glDisable(GL_LIGHT1);
	}

	if (b4View){
		glMatrixMode(GL_MODELVIEW);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		glPushMatrix();
		gluLookAt(
			camera_dis*sinf(camera_angle*3.14159 / 180.0f),
			camera_height,
			camera_dis*cosf(camera_angle*3.14159 / 180.0f),
			0,
			1,
			0,
			0,
			1,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		
		glViewport(0, screenWidth / 2, screenWidth / 2, screenHeight / 2);

		//glLoadIdentity();
		glPushMatrix();
		glRotatef(base.rotateY, 0, 1, 0);
		drawBase();
		drawColumn();
		drawT1();
		drawT2();
		drawShelf1();
		drawShelf2();
		drawCrank();
		glPopMatrix();
		glPopMatrix();


		
		glPushMatrix();
		gluLookAt(
			4,
			1,
			0,
			0,
			1,
			0,
			0,
			1,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		glViewport(screenWidth / 2, screenWidth / 2, screenWidth / 2, screenHeight / 2);

		//glLoadIdentity();
		glPushMatrix();
		glRotatef(base.rotateY, 0, 1, 0);
		drawBase();
		drawColumn();
		drawT1();
		drawT2();
		drawShelf1();
		drawShelf2();
		drawCrank();
		glPopMatrix();
		glPopMatrix();

		glPushMatrix();
		gluLookAt(
			0,
			1,
			4,
			0,
			1,
			0,
			0,
			1,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		glViewport(0, 0, screenWidth / 2, screenHeight / 2);

		//glLoadIdentity();
		glPushMatrix();
		glRotatef(base.rotateY, 0, 1, 0);
		drawBase();
		drawColumn();
		drawT1();
		drawT2();
		drawShelf1();
		drawShelf2();
		drawCrank();
		glPopMatrix();
		glPopMatrix();

		glPushMatrix();
		gluLookAt(
			0,
			5.5,
			0,
			0,
			1,
			0,
			1,
			0,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		glViewport(screenWidth / 2, 0, screenWidth / 2, screenHeight / 2);

		//glLoadIdentity();
		glPushMatrix();
		glRotatef(base.rotateY, 0, 1, 0);
		drawBase();
		drawColumn();
		drawT1();
		drawT2();
		drawShelf1();
		drawShelf2();
		drawCrank();
		glPopMatrix();
	}
	else{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		gluLookAt(
			camera_dis*sinf(camera_angle*3.14159 / 180.0f),
			camera_height,
			camera_dis*cosf(camera_angle*3.14159 / 180.0f),
			0,
			1,
			0,
			0,
			1,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, screenWidth, screenHeight);

		//glLoadIdentity();
		glPushMatrix();
		glRotatef(base.rotateY, 0, 1, 0);
		drawBase();
		drawColumn();
		drawT1();
		drawT2();
		drawShelf1();
		drawShelf2();
		drawCrank();
		glPopMatrix();
	}

	


	//drawAxis();
	drawFloor();
	glPopMatrix();
	glFlush();
	glutSwapBuffers();

}
void processTimer(int value){
	glutTimerFunc(100, processTimer, value);
	glutPostRedisplay();
}
void myDisplay1()
{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPushMatrix();
		camera_dis = 4;
		gluLookAt(
			camera_dis*sinf(camera_angle*3.14159 / 180.0f),
			camera_height,
			camera_dis*cosf(camera_angle*3.14159 / 180.0f),
			0,
			1,
			0,
			0,
			1,
			0
			);
		//gluLookAt(-6, 4, 0, 0, 1, 0, 0, 1, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, screenWidth, screenHeight);
		glColor3f(1.0, 0.0, 0.0);
		//glLoadIdentity();
		int numberOfPoint = 72;
		float u = 0;
		float v = -1;
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBegin(GL_TRIANGLE_STRIP);
		for (int i = 0; i < numberOfPoint; i++){
			for (int j = -10; j <= 10; j ++){
				v = j*2.0f / 20.0f;
				u = i*(360 / numberOfPoint)*3.14159 / 180.0f;
				glVertex3f(cosf(u)*(1 + (v / 2)*cosf(u / 2)), sinf(u)*(1 + (v / 2)*cosf(u / 2)), (v / 2)*sinf(u / 2));
				u = (i+1)*(360 / numberOfPoint)*3.14159 / 180.0f;
				glVertex3f(cosf(u)*(1 + (v / 2)*cosf(u / 2)), sinf(u)*(1 + (v / 2)*cosf(u / 2)), (v / 2)*sinf(u / 2));
			}
			
		}
		glEnd();

		/*glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(1, 1, 0);
		glEnd();*/

		//drawAxis();
		glPopMatrix();
		glFlush();
		glutSwapBuffers();

}
void draw(){
	base.CreateCylinder(baseRadius, baseHeight, 36);
	base.SetColor(0);

	column.CreateCuboid(columnSizeZ, columnSizeY, columnSizeZ);
	column.SetColor(1);

	columnT1.CreateCuboid(columnSizeX, 0.9, columnSizeZ);
	columnT1.SetColor(2);
	ovalT1.CreateOvalWithHole(0.15, 0.1, 0.5, 0.4, 0.1, 36);
	ovalT1.SetColor(2);

	columnT2.CreateCuboid(columnSizeX, 0.9, columnSizeZ);
	columnT2.SetColor(3);
	ovalT2.CreateOvalWithHole(0.15, 0.1, 0.5, 0.4, 0.1, 36);
	ovalT2.SetColor(3);

	cuboicShelf1.CreateCuboid(0.1, 0.1, 0.2);
	cuboicShelf1.SetColor(4);
	cuboicWithHoleShelf1.createCuboidWithHole(0.15, 0.15, 0.15, 0.1, 0.1, 0.1);
	cuboicWithHoleShelf1.SetColor(4);

	cuboic1Shelf2.CreateCuboid(1, 0.1, 0.1);
	cuboic1Shelf2.SetColor(5);
	cuboic2Shelf2.CreateCuboid(0.1, 0.1, 0.1);
	cuboic2Shelf2.SetColor(5);
	cuboicWithHoleShelf2.createCuboidWithHole(0.15, 0.15, 0.15, 0.1, 0.1, 0.1);
	cuboicWithHoleShelf2.SetColor(5);

	cuboic1Crank.CreateCylinder(0.1, 0.3, 36);
	cuboic1Crank.SetColor(6);
	cuboic2Crank.CreateCylinder(0.1, 0.1, 36);
	cuboic2Crank.SetColor(1);
	ovalCrank.CreateOval(0.1, 0.2, 0.125, 36);
	ovalCrank.SetColor(6);
}


int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 3 - Demo (2017-2018)"); // open the screen window

	draw();


	myInit();

	glutKeyboardFunc(myKeyboard);
	glutDisplayFunc(myDisplay);
	glutTimerFunc(100, processTimer, 3);
	glutSpecialFunc(mySpecialKeyboard);
	glutMainLoop();
	return 0;
}

