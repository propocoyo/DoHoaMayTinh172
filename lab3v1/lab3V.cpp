// Lab3.cpp : Defines the entry point for the console application.
//
//ver1.1

#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"

using namespace std;

#define PI			3.1415926

int		screenWidth = 600;
int		screenHeight = 600;

bool	bWireFrame = false;

float	baseRadius = 0.8;
float	baseHeight = 0.2;
float	baseRotateStep = 5;

float	columnSizeX = 0.25;
float	columnSizeZ = columnSizeX;
float	columnSizeY = 5;

Mesh	base;
Mesh	column;
Mesh	thanh_T1_1, thanh_T1_2;
Mesh	giado1_hop, giado1_hop_rong;
Mesh	giado2_hop, giado2_hop_rong;
Mesh	tayquay_tru, tayquay_ovan;

//...................//

float baseSizeX_giado1_1 = 0.25;
float baseSizeY_giado1_1 = 0.25;
float baseSizeZ_giado1_1 = 0.4;

float baseSizeX_giado1_2 = 0.35;
float baseSizeY_giado1_2 = 0.35;
float baseSizeZ_giado1_2 = 0.35;
float basea_giado1_2 = 0.05;

float baseSizeX_thanhT1_1 = 0.25;
float baseSizeY_thanhT1_1 = columnSizeY *2/ 3;
float baseSizeZ_thanhT1_1 = 0.25;


float baseSizeLen_thanhT1_2 = columnSizeY / 2;
float baseSizeH_thanhT1_2 = 0.25;
float baseSizeR_thanhT1_2 = 0.2;
float baseSizeRR_thanhT1_2 = 0.05;

float baselen_TayQ_ovan = columnSizeY / 7;
float baser_TayQ_ovan = 0.15;
float baseh_TayQ_ovan = 0.1;

float baseh_TayQ_tru = 0.8;
float baser_TayQ_tru = 0.15 / 2;

void myKeyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case '1':
		base.rotateY += baseRotateStep;
		if (base.rotateY > 360)
			base.rotateY -= 360;
		break;
	case '2':
		base.rotateY -= baseRotateStep;
		if (base.rotateY < 0)
			base.rotateY += 360;
		break;
	case '3':
		tayquay_tru.rotateZ += baseRotateStep;
		if (tayquay_tru.rotateZ > 360)
			tayquay_tru.rotateZ -= 360;
		break;
	case '4':
		tayquay_tru.rotateZ -= baseRotateStep;
		if (tayquay_tru.rotateZ < 0)
			tayquay_tru.rotateZ += 360;
		break;
	case 'w':
		if (bWireFrame) bWireFrame = false;
		else bWireFrame = true;
		break;
	case 'W':
		if (bWireFrame) bWireFrame = false;
		else bWireFrame = true;
		break;
	}
	glutPostRedisplay();
}
void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);//x
	glVertex3f(4, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);//y
	glVertex3f(0, 4, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);//z
	glVertex3f(0, 0, 4);
	glEnd();
}

void drawBase()
{
	glPushMatrix();

	glTranslated(0, baseHeight / 2.0, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		base.DrawWireframe();
	else
		base.DrawColor();

	glPopMatrix();
}
void drawColumn()
{
	glPushMatrix();

	glTranslated(0, baseHeight / 2.0 + columnSizeY / 2, 0);
	//glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		column.DrawWireframe();
	else
		column.DrawColor();

	glPopMatrix();
}
void drawGiaDo1(){
	glPushMatrix();
	glTranslated(0, 2 * columnSizeY / 3, baseSizeZ_giado1_1 / 2 + columnSizeX / 2);
	if (bWireFrame)
		giado1_hop.DrawWireframe();
	else
		giado1_hop.DrawColor();
	glPopMatrix();
	glPushMatrix();
	glTranslated(0, 2 * columnSizeY / 3, baseSizeZ_giado1_1 / 2 + columnSizeX / 2 + baseSizeZ_giado1_2);
	if (bWireFrame)
		giado1_hop_rong.DrawWireframe();
	else
		giado1_hop_rong.DrawColor();
	glPopMatrix();
}
void drawThanhT1_1(){
	glPushMatrix();
	glTranslated(0,-baselen_TayQ_ovan*sin((tayquay_tru.rotateZ-170)*PI/180)-0.2,0);
	glTranslated(0, columnSizeY * 3.9 / 5, 0.25 + baseSizeZ_giado1_1 + basea_giado1_2);
	if (bWireFrame)
		thanh_T1_1.DrawWireframe();
	else
		thanh_T1_1.DrawColor();
	glPopMatrix();
}
void drawThanhT1_2(){
	glPushMatrix();
	glTranslated(0,-baselen_TayQ_ovan*sin((tayquay_tru.rotateZ-170)*PI/180)-0.2,0);
	glTranslated(0, 3.9 * columnSizeY / 5 - baseSizeY_thanhT1_1 / 2 - baseSizeH_thanhT1_2, 0.25 + baseSizeZ_giado1_1 + basea_giado1_2);
	glRotated(90, 1, 0, 0);

	if (bWireFrame)
		thanh_T1_2.DrawWireframe();
	else
		thanh_T1_2.DrawColor();

	glPopMatrix();

}
void drawThanhT1(){

	drawThanhT1_1();
	drawThanhT1_2();
}
void drawTayQuayOvan(){
	glPushMatrix();
	
	glTranslated(0 , columnSizeY / 3, columnSizeZ/ 2 + baseh_TayQ_ovan / 2);
	//glRotated(tayquay_tru.rotateZ,0,0,1);
	//glTranslated(-(baselen_TayQ_ovan/2+baser_TayQ_ovan/2)*sin(15.0*PI/180) , 0, columnSizeZ/ 2 + baseh_TayQ_ovan / 2);
	
	
	glRotated(tayquay_tru.rotateZ,0,0,1);
	glTranslated(+baselen_TayQ_ovan/2,0,0);
	glRotated(90, 1, 0, 0);
	if (bWireFrame){
		tayquay_ovan.DrawWireframe();
		glPushMatrix();
		glTranslated(baselen_TayQ_ovan / 2, baseh_TayQ_tru/2, 0);
		tayquay_tru.DrawWireframe();
		glPopMatrix();
	}
	else
	{
		tayquay_ovan.DrawColor();
		glPushMatrix();
		glTranslated(+baselen_TayQ_ovan / 2, baseh_TayQ_tru / 2, 0);
		tayquay_tru.DrawColor();
		glPopMatrix();
	}
	glPopMatrix();
}

void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(6, 4, 6, 0, 1, 0, 0, 1, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, screenWidth, screenHeight);
	drawAxis();
		glRotatef(base.rotateY, 0, 1, 0);
	drawBase();
	drawColumn();
	drawGiaDo1();
	drawThanhT1();
	drawTayQuayOvan();
	glFlush();
	glutSwapBuffers();
}


void myInit()
{
	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 3 - Demo (2017-2018)"); // open the screen window

	base.CreateOvan(1, 0, 0.2);
	base.SetColor(6);

	column.CreateCuboid(columnSizeZ, columnSizeY, columnSizeZ);
	column.SetColor(1);

	giado1_hop.CreateCuboid(baseSizeX_giado1_1, baseSizeY_giado1_1, baseSizeZ_giado1_1);
	giado1_hop.SetColor(2);
	giado1_hop_rong.CreateEx2(baseSizeX_giado1_2, baseSizeY_giado1_2, baseSizeZ_giado1_2, basea_giado1_2);
	giado1_hop_rong.SetColor(2);

	thanh_T1_1.CreateCuboid(baseSizeX_thanhT1_1, baseSizeY_thanhT1_1, baseSizeZ_thanhT1_1);
	thanh_T1_1.SetColor(3);
	thanh_T1_2.CreateOvan2(baseSizeR_thanhT1_2, baseSizeLen_thanhT1_2, baseSizeH_thanhT1_2, baseSizeRR_thanhT1_2 );
	thanh_T1_2.SetColor(3);

	tayquay_ovan.CreateOvan(baser_TayQ_ovan, baselen_TayQ_ovan, baseh_TayQ_ovan);
	tayquay_ovan.SetColor(4);
	tayquay_tru.CreateHinhtru(baser_TayQ_tru, baseh_TayQ_tru);
	tayquay_tru.rotateZ=170;
	tayquay_tru.SetColor(9);

	myInit();
	glutKeyboardFunc(myKeyboard);
	glutDisplayFunc(myDisplay);

	glutMainLoop();
	return 0;
}

