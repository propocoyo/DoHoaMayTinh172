#include "stdafx.h"
#include "Mesh.h"
#include <math.h>

#define PI			3.1415926
#define	COLORNUM		14


float	ColorArr[COLORNUM][3] = { { 1.0, 0.0, 0.0 },{ 0.0, 1.0, 0.0 },{ 0.0,  0.0, 1.0 },
{ 1.0, 1.0,  0.0 },{ 1.0, 0.0, 1.0 },{ 0.0, 1.0, 1.0 },
{ 0.3, 0.3, 0.3 },{ 0.5, 0.5, 0.5 },{ 0.9,  0.9, 0.9 },
{ 1.0, 0.5,  0.5 },{ 0.5, 1.0, 0.5 },{ 0.5, 0.5, 1.0 },
{ 0.0, 0.0, 0.0 },{ 1.0, 1.0, 1.0 } };





void Mesh::CreateCube(float	fSize)
{
	int i;

	numVerts = 8;
	pt = new Point3[numVerts];
	pt[0].set(-fSize, fSize, fSize);
	pt[1].set(fSize, fSize, fSize);
	pt[2].set(fSize, fSize, -fSize);
	pt[3].set(-fSize, fSize, -fSize);
	pt[4].set(-fSize, -fSize, fSize);
	pt[5].set(fSize, -fSize, fSize);
	pt[6].set(fSize, -fSize, -fSize);
	pt[7].set(-fSize, -fSize, -fSize);


	numFaces = 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;
	for (i = 0; i<face[2].nVerts; i++)
		face[2].vert[i].colorIndex = 2;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	for (i = 0; i<face[3].nVerts; i++)
		face[3].vert[i].colorIndex = 3;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;
	for (i = 0; i<face[5].nVerts; i++)
		face[5].vert[i].colorIndex = 5;

}


void Mesh::CreateTetrahedron()
{
	int i;
	numVerts = 4;
	pt = new Point3[numVerts];
	pt[0].set(0, 0, 0);
	pt[1].set(1, 0, 0);
	pt[2].set(0, 1, 0);
	pt[3].set(0, 0, 1);

	numFaces = 4;
	face = new Face[numFaces];

	face[0].nVerts = 3;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 2;
	face[0].vert[2].vertIndex = 3;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;


	face[1].nVerts = 3;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;


	face[2].nVerts = 3;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 3;
	face[2].vert[2].vertIndex = 2;
	for (i = 0; i<face[2].nVerts; i++)
		face[2].vert[i].colorIndex = 2;


	face[3].nVerts = 3;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 1;
	face[3].vert[1].vertIndex = 3;
	face[3].vert[2].vertIndex = 0;
	for (i = 0; i<face[3].nVerts; i++)
		face[3].vert[i].colorIndex = 3;
}


void Mesh::DrawWireframe()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::DrawColor()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			int		ic = face[f].vert[v].colorIndex;

			ic = f % COLORNUM;

			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}


//---------- Exercise 1--------------//

void Mesh::CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ)
{

	int i;

	numVerts = 8;
	pt = new Point3[numVerts];
	pt[0].set(-fSizeX / 2, -fSizeY / 2, fSizeZ / 2);
	pt[1].set(-fSizeX / 2, -fSizeY / 2, -fSizeZ / 2);
	pt[2].set(-fSizeX / 2, fSizeY / 2, -fSizeZ / 2);
	pt[3].set(-fSizeX / 2, fSizeY / 2, fSizeZ / 2);
	pt[4].set(fSizeX / 2, -fSizeY / 2, fSizeZ / 2);
	pt[5].set(fSizeX / 2, -fSizeY / 2,-fSizeZ / 2);
	pt[6].set(fSizeX / 2, fSizeY / 2, -fSizeZ / 2);
	pt[7].set(fSizeX / 2, fSizeY / 2, fSizeZ / 2);


	numFaces = 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 7;
	face[0].vert[1].vertIndex = 6;
	face[0].vert[2].vertIndex = 5;
	face[0].vert[3].vertIndex = 4;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 3;
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	face[1].vert[3].vertIndex = 0;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 3;
	face[2].vert[1].vertIndex = 2;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[3].vertIndex = 7;
	for (i = 0; i<face[2].nVerts; i++)
		face[2].vert[i].colorIndex = 2;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 0;
	face[3].vert[1].vertIndex = 1;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	for (i = 0; i<face[3].nVerts; i++)
		face[3].vert[i].colorIndex = 3;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 0;
	face[4].vert[1].vertIndex = 3;
	face[4].vert[2].vertIndex = 7;
	face[4].vert[3].vertIndex = 4;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 1;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 5;
	for (i = 0; i<face[5].nVerts; i++)
		face[5].vert[i].colorIndex = 5;

}
//---------- Exercise 2--------------//

void Mesh::CreateEx2(float	fSizeX, float fSizeY, float	fSizeZ, float a)
{

	int i;

	numVerts = 16;
	pt = new Point3[numVerts];
	pt[0].set(-fSizeX / 2, -fSizeY / 2, fSizeZ / 2);
	pt[1].set(-fSizeX / 2, -fSizeY / 2, -fSizeZ / 2);
	pt[2].set(-fSizeX / 2, fSizeY / 2, -fSizeZ / 2);
	pt[3].set(-fSizeX / 2, fSizeY / 2, fSizeZ / 2);
	pt[4].set(fSizeX / 2, -fSizeY / 2, fSizeZ / 2);
	pt[5].set(fSizeX / 2, -fSizeY / 2,-fSizeZ / 2);
	pt[6].set(fSizeX / 2, fSizeY / 2, -fSizeZ / 2);
	pt[7].set(fSizeX / 2, fSizeY / 2, fSizeZ / 2);

	pt[8].set(-fSizeX / 2+a, -fSizeY / 2, fSizeZ / 2-a);
	pt[9].set(-fSizeX / 2+a, -fSizeY / 2, -fSizeZ / 2+a);
	pt[10].set(-fSizeX / 2+a, fSizeY / 2, -fSizeZ / 2+a);
	pt[11].set(-fSizeX / 2+a, fSizeY / 2, fSizeZ / 2-a);
	pt[12].set(fSizeX / 2-a, -fSizeY / 2, fSizeZ / 2-a);
	pt[13].set(fSizeX / 2-a, -fSizeY / 2,-fSizeZ / 2+a);
	pt[14].set(fSizeX / 2-a, fSizeY / 2, -fSizeZ / 2+a);
	pt[15].set(fSizeX / 2-a, fSizeY / 2, fSizeZ / 2-a);


	numFaces = 16;
	face = new Face[numFaces];

	//4face out
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 7;
	face[0].vert[1].vertIndex = 6;
	face[0].vert[2].vertIndex = 5;
	face[0].vert[3].vertIndex = 4;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 3;
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	face[1].vert[3].vertIndex = 0;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 3;
	face[2].vert[2].vertIndex = 7;
	face[2].vert[3].vertIndex = 4;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 1;
	face[3].vert[1].vertIndex = 2;
	face[3].vert[2].vertIndex = 6;
	face[3].vert[3].vertIndex = 5;
	for (i = 0; i<face[5].nVerts; i++)
		face[5].vert[i].colorIndex = 5;

	//4face inside
	int con=8;
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 7+con;
	face[4].vert[1].vertIndex = 6+con;
	face[4].vert[2].vertIndex = 5+con;
	face[4].vert[3].vertIndex = 4+con;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3+con;
	face[5].vert[1].vertIndex = 2+con;
	face[5].vert[2].vertIndex = 1+con;
	face[5].vert[3].vertIndex = 0+con;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	face[6].nVerts = 4;
	face[6].vert = new VertexID[face[6].nVerts];
	face[6].vert[0].vertIndex = 0+con;
	face[6].vert[1].vertIndex = 3+con;
	face[6].vert[2].vertIndex = 7+con;
	face[6].vert[3].vertIndex = 4+con;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	face[7].nVerts = 4;
	face[7].vert = new VertexID[face[7].nVerts];
	face[7].vert[0].vertIndex = 1+con;
	face[7].vert[1].vertIndex = 2+con;
	face[7].vert[2].vertIndex = 6+con;
	face[7].vert[3].vertIndex = 5+con;
	for (i = 0; i<face[5].nVerts; i++)
		face[5].vert[i].colorIndex = 5;

	//4face top
	face[8].nVerts = 4;
	face[8].vert = new VertexID[face[8].nVerts];
	face[8].vert[0].vertIndex = 7;
	face[8].vert[1].vertIndex = 3;
	face[8].vert[2].vertIndex = 3+con;
	face[8].vert[3].vertIndex = 7+con;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;


	face[9].nVerts = 4;
	face[9].vert = new VertexID[face[9].nVerts];
	face[9].vert[0].vertIndex = 3;
	face[9].vert[1].vertIndex = 2;
	face[9].vert[2].vertIndex = 2+con;
	face[9].vert[3].vertIndex = 3+con;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	face[10].nVerts = 4;
	face[10].vert = new VertexID[face[10].nVerts];
	face[10].vert[0].vertIndex = 2;
	face[10].vert[1].vertIndex = 6;
	face[10].vert[2].vertIndex = 6+con;
	face[10].vert[3].vertIndex = 2+con;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	face[11].nVerts = 4;
	face[11].vert = new VertexID[face[11].nVerts];
	face[11].vert[0].vertIndex = 7;
	face[11].vert[1].vertIndex = 6;
	face[11].vert[2].vertIndex = 6+con;
	face[11].vert[3].vertIndex = 7+con;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	//face bottom
	face[12].nVerts = 4;
	face[12].vert = new VertexID[face[12].nVerts];
	face[12].vert[0].vertIndex = 7-3;
	face[12].vert[1].vertIndex = 3-3;
	face[12].vert[2].vertIndex = 3+con-3;
	face[12].vert[3].vertIndex = 7+con-3;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;


	face[13].nVerts = 4;
	face[13].vert = new VertexID[face[13].nVerts];
	face[13].vert[0].vertIndex = 3-3;
	face[13].vert[1].vertIndex = 2-1;
	face[13].vert[2].vertIndex = 2+con-1;
	face[13].vert[3].vertIndex = 3+con-3;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	face[14].nVerts = 4;
	face[14].vert = new VertexID[face[14].nVerts];
	face[14].vert[0].vertIndex = 2-1;
	face[14].vert[1].vertIndex = 6-1;
	face[14].vert[2].vertIndex = 6+con-1;
	face[14].vert[3].vertIndex = 2+con-1;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	face[15].nVerts = 4;
	face[15].vert = new VertexID[face[15].nVerts];
	face[15].vert[0].vertIndex = 7-3;
	face[15].vert[1].vertIndex = 6-1;
	face[15].vert[2].vertIndex = 6+con-1;
	face[15].vert[3].vertIndex = 7+con-3;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

}

//---------- Exercise 3--------------//

void Mesh::CreateHinhtru(float r, float h)
{
	int i;
	float D2R = 3.14159f / 180;

	numVerts = 74;
	pt = new Point3[numVerts];
	pt[0].set(0, h / 2, 0);
	pt[73].set(0, -h / 2, 0);
	int k;

	for (int j = 1; j<37; j = j ++)
	{
		pt[j].set(r*cos((j-1)*10*D2R), h / 2, r*sin((j-1)*10*D2R));
	}
	for (int j = 0; j<360; j = j + 10)
	{
		k = 37 + j / 10;
		pt[k].set(r*cos(j*D2R), -h / 2, r*sin(j*D2R));
	}

	numFaces = 109;
	face = new Face[numFaces];
	//top
	for (int j = 1; j < 37; j++)
	{
		face[j].nVerts = 3;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 0;
		face[j].vert[1].vertIndex = j;
		if(j!=36) face[j].vert[2].vertIndex = j + 1;
		else face[j].vert[2].vertIndex = 1;
		for (i = 0; i<face[j].nVerts; i++)
			face[j].vert[i].colorIndex = j;
	}
	//bottom
	for (int j = 37; j<73; j++)
	{
		face[j].nVerts = 3;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 73;
		face[j].vert[1].vertIndex = j;
		if(j!=72) face[j].vert[2].vertIndex = j + 1;
		else face[j].vert[2].vertIndex = 37;
		for (i = 0; i<face[j].nVerts; i++)
			face[j].vert[i].colorIndex = j;

	}
	//Than
	for (int j = 1; j <= 35; j++)
	{
		k = j + 72;
		face[k].nVerts = 4;
		face[k].vert = new VertexID[face[k].nVerts];
		face[k].vert[0].vertIndex = j;
		face[k].vert[1].vertIndex = j + 1;
		face[k].vert[2].vertIndex = j + 37;
		face[k].vert[3].vertIndex = j + 36;
		for (i = 0; i<face[k].nVerts; i++)
			face[k].vert[i].colorIndex = k;
	}
	//than end
	face[108].nVerts = 4;
	face[108].vert = new VertexID[face[108].nVerts];
	face[108].vert[0].vertIndex = 1;
	face[108].vert[1].vertIndex = 36;
	face[108].vert[2].vertIndex = 72;
	face[108].vert[3].vertIndex = 37;
	for (i = 0; i<face[108].nVerts; i++)
		face[108].vert[i].colorIndex = 108;
}
void Mesh::CreateOvan(float r, float len, float h){
	float D2R = 3.14159f / 180;
	numVerts= 19*4;
	pt = new Point3[numVerts];

	for( int i= 0; i<=18; i++){
		 pt[i].set(len/2+ r*cos((270+i*10)*D2R),h/2,r*cos(i*10*D2R));
	}
	for (int i=0; i<=18; i++){
		int k=0;
		k=19+i;
		pt[k].set(-pt[i].x,pt[i].y,pt[i].z);
	}
	for (int i =0;i< 38; i++){
		int k;
		k=38+i;
		pt[k].set(pt[i].x,-pt[i].y,pt[i].z);
	}
	numFaces = 40;
	face = new Face[numFaces];

	for (int j = 0; j<18; j++)
	{
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = j;
		face[j].vert[1].vertIndex = j+1;
		face[j].vert[2].vertIndex = j+39;
		face[j].vert[3].vertIndex = j+38;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
	for (int l = 0; l<18; l++)
	{
		int j;
		j=l+18;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = j+1;
		face[j].vert[1].vertIndex = j+1+1;
		face[j].vert[2].vertIndex = j+39+1;
		face[j].vert[3].vertIndex = j+38+1;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
	int j;
	j=36;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 0;
		face[j].vert[1].vertIndex = 38;
		face[j].vert[2].vertIndex = 57;
		face[j].vert[3].vertIndex = 19;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=37;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 18;
		face[j].vert[1].vertIndex = 37;
		face[j].vert[2].vertIndex = 75;
		face[j].vert[3].vertIndex = 56;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=38;
		face[j].nVerts = 38;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i= 0; i<38; i++){
		if (i<19) face[j].vert[i].vertIndex = i;
		else face[j].vert[i].vertIndex = 37+19-i;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=39;
		face[j].nVerts = 38;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i= 0; i<38; i++){
		if (i<19) face[j].vert[i].vertIndex = i+38;
		else face[j].vert[i].vertIndex = 75+57-i-38;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
}
void Mesh::CreateOvan2(float r, float len, float h,float rr){
	float D2R = 3.14159f / 180;
	numVerts= 19*4*2;
	pt = new Point3[numVerts];

	for( int i= 0; i<=18; i++){
		 pt[i].set(len/2+ r*cos((270+i*10)*D2R),h/2,r*cos(i*10*D2R));
	}
	for (int i=0; i<=18; i++){
		int k=0;
		k=19+i;
		pt[k].set(-pt[i].x,pt[i].y,pt[i].z);
	}
	for (int i =0;i< 38; i++){
		int k;
		k=38+i;
		pt[k].set(pt[i].x,-pt[i].y,pt[i].z);
	}
	// ovan trong
	for( int i= 0; i<=18; i++){
		 int j=0;
		 j=i+76;
		 pt[j].set(len/2+ rr*cos((270+i*10)*D2R),h/2,rr*cos(i*10*D2R));
	}
	for (int i=0; i<=18; i++){
		int k=0;
		k=i+95;
		pt[k].set(-pt[i+76].x,pt[i+76].y,pt[i+76].z);
	}
	for (int i =0;i< 38; i++){
		int k;
		k=114+i;
		pt[k].set(pt[i+76].x,-pt[i+76].y,pt[i+76].z);
	}



	// face ngoai cung 
	numFaces = 40+36*4+4+18*2;
	face = new Face[numFaces];

	for (int j = 0; j<18; j++)
	{
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = j;
		face[j].vert[1].vertIndex = j+1;
		face[j].vert[2].vertIndex = j+39;
		face[j].vert[3].vertIndex = j+38;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
	for (int l = 0; l<18; l++)
	{
		int j;
		j=l+18;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = j+1;
		face[j].vert[1].vertIndex = j+1+1;
		face[j].vert[2].vertIndex = j+39+1;
		face[j].vert[3].vertIndex = j+38+1;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
	int j;
	j=36;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 0;
		face[j].vert[1].vertIndex = 38;
		face[j].vert[2].vertIndex = 57;
		face[j].vert[3].vertIndex = 19;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=37;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 18;
		face[j].vert[1].vertIndex = 37;
		face[j].vert[2].vertIndex = 75;
		face[j].vert[3].vertIndex = 56;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=38;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 76;
		face[j].vert[1].vertIndex = 38+76;
		face[j].vert[2].vertIndex = 57+76;
		face[j].vert[3].vertIndex = 19+76;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		j=39;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = 76+18;
		face[j].vert[1].vertIndex = 38+76+18;
		face[j].vert[2].vertIndex = 57+76+18;
		face[j].vert[3].vertIndex = 19+76+18;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		
		for (int k=0; k< 18; k++){
		int j;
		j=k+40;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = k;
		face[j].vert[1].vertIndex = k+1;
		face[j].vert[2].vertIndex = k+77;
		face[j].vert[3].vertIndex = k+76;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		}

		for (int k=0; k< 18; k++){
		int j;
		j=k+40+18;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = k+38;
		face[j].vert[1].vertIndex = k+1+38;
		face[j].vert[2].vertIndex = k+77+38;
		face[j].vert[3].vertIndex = k+76+38;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		}
		for (int k=0; k< 18; k++){
		int j;
		j=k+40+18*2;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = k+19;
		face[j].vert[1].vertIndex = k+1+19;
		face[j].vert[2].vertIndex = k+77+19;
		face[j].vert[3].vertIndex = k+76+19;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		}
		for (int k=0; k< 18; k++){
		int j;
		j=k+40+18*3;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = k+57;
		face[j].vert[1].vertIndex = k+1+57;
		face[j].vert[2].vertIndex = k+77+57;
		face[j].vert[3].vertIndex = k+76+57;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		}

		j=184;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = 0;
		face[j].vert[1].vertIndex = 19;
		face[j].vert[2].vertIndex = 95;
		face[j].vert[3].vertIndex = 76;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=185;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = 0+38;
		face[j].vert[1].vertIndex = 19+38;
		face[j].vert[2].vertIndex = 95+38;
		face[j].vert[3].vertIndex = 76+38;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
		
		j=186;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = 0+18;
		face[j].vert[1].vertIndex = 19+18;
		face[j].vert[2].vertIndex = 95+18;
		face[j].vert[3].vertIndex = 76+18;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;

		j=187;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		for (int i = 0; i<face[j].nVerts; i++){
		face[j].vert[0].vertIndex = 0+18+38;
		face[j].vert[1].vertIndex = 19+18+38;
		face[j].vert[2].vertIndex = 95+18+38;
		face[j].vert[3].vertIndex = 76+18+38;
		}
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;



		for (int l = 0; l<18; l++)
	{
		int j=0;
		j=l+188;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = l+76;
		face[j].vert[1].vertIndex = l+76+1;
		face[j].vert[2].vertIndex = l+114+1;
		face[j].vert[3].vertIndex = l+114;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
	for (int l = 0; l<18; l++)
	{
		int j=0;
		j=l+188+18;
		face[j].nVerts = 4;
		face[j].vert = new VertexID[face[j].nVerts];
		face[j].vert[0].vertIndex = l+76+19;
		face[j].vert[1].vertIndex = l+76+1+19;
		face[j].vert[2].vertIndex = l+114+1+19;
		face[j].vert[3].vertIndex = l+114+19;
		for (int i = 0; i<face[j].nVerts; i++)
		face[j].vert[i].colorIndex = j;
	}
}

