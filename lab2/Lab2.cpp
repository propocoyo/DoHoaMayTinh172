// Bai3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"

using namespace std;

int		screenWidth = 600;
int		screenHeight= 300;

Mesh	tetrahedron;
Mesh	cube;
Mesh	C1;
Mesh	C2;
Mesh	C3;
Mesh	C4;

int		nChoice = 1;

void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(4, 0, 0);

		glVertex3f(0, 0, 0);
		glVertex3f(0, 4, 0);

		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, 4);
	glEnd();
}
void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(4.5, 4, 2, 0, 0, 0, 0, 1, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0, 0, screenWidth/2, screenHeight);
	
	drawAxis();

	glColor3f(0, 0, 0);
	switch (nChoice)
	{
	case 1:
		tetrahedron.DrawWireframe();
		break;
	case 2:
		cube.DrawWireframe();
		break;
	case 3:
		C1.DrawWireframe();
		break;
	case 4:
		C2.DrawWireframe();
		break;
	case 5:
		C3.DrawWireframe();
		break;
	case 6:
		C4.DrawWireframe();
		break;
	}
	glViewport(screenWidth/2, 0, screenWidth/2, screenHeight);

	drawAxis();
	switch (nChoice)
	{
	case 1:
		tetrahedron.DrawColor();
		break;
	case 2:
		cube.DrawColor();
		break;
	case 3:
		C1.DrawColor();
		break;
	case 4:
		C2.DrawColor();
		break;
	case 5:
		C3.DrawColor();
		break;
	case 6:
		C4.DrawColor();
		break;

	}
	glFlush();
    glutSwapBuffers();
}

void myInit()
{
	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "1. ovan1" << endl;
	cout << "2. Cube" << endl;
	cout << "3. Cuboid" << endl;
	cout << "4. Double Cuboid" << endl;
	cout << "5. Hinh tru" << endl;
	cout << "6. Ovan2" << endl;
	cout << "Input the choice: " << endl;
	cin  >> nChoice;

	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 2"); // open the screen window

	tetrahedron.CreateOvan(2,5,3);
	cube.CreateCube(1);
	C1.CreateCuboid(1,2,3);
	C2.CreateEx2(2, 3, 4, 0.5);
	C3.CreateHinhtru(2, 3);
	C4.CreateOvan2(2, 5, 2,1);
	myInit();
    glutDisplayFunc(myDisplay);
	  
	glutMainLoop();
	return 0;
}

